# How to setup Python Plots

Just run `make install` and try with `make demo`.  
This will run all different plot wrappers with some simple calls in [example_plots](example/example_plots.py).

## matplotlib

If there is the message when using `.show()`  
`UserWarning: Matplotlib is currently using agg, which is a non-GUI backend, so cannot show the figure.`
  
You have to install tkinter:
- Ubuntu: `sudo apt-get install python3-tk`

