""" Example for Simple Plots """

import os

from python_plots.plotit import PlotHandler
from python_plots.plotit_altair import AltairHandler
from python_plots.plotit_bokeh import BokehHandler
from python_plots.plotit_holoviews import HoloviewsHandler
from python_plots.plotit_hvplot import HVPlotHandler
from python_plots.plotit_matplotlib import MatplotlibHandler
from python_plots.plotit_plotly import PlotlyHandler
from python_plots.plotit_seaborn import SeabornHandler

from example_data import x_values, sample_a, sample_b, sample_c


# Support functions #
#####################

def save(p_handler: PlotHandler, file_type: str):
    try:
        p_handler.save(os.path.join(folder_name, p_handler.handler_name + '.' + file_type))
    except ValueError as err:
        print(' {} not possible for {}: {}'.format(file_type, p_handler.handler_name, err))
    except RuntimeError as err:
        print(' {} not possible for {}: {}'.format(file_type, p_handler.handler_name, err))


def show(p_handler: PlotHandler):
    try:
        p_handler.show()
    except ValueError as err:
        print(' Visualization not possible for {}: {}'.format(p_handler.handler_name, err))
    except PermissionError as err:
        print(' Visualization not possible for {}: {}'.format(p_handler.handler_name, err))


# Plot Generation #
###################

if __name__ == '__main__':

    folder_name = 'Plots'
    if not os.path.exists(folder_name):
        os.mkdir(folder_name)

    for plot_handler in [AltairHandler(),
                         BokehHandler(),
                         HoloviewsHandler(),
                         HVPlotHandler(),
                         MatplotlibHandler(),
                         PlotlyHandler(),
                         SeabornHandler()]:  # GGPlotHandler()

        print('Using {}'.format(plot_handler.handler_name))

        plot_handler.prepare_plot()

        plot_handler.scatter(x_values, sample_a, 'Sample A')
        plot_handler.scatter(x_values, sample_b, 'Sample B')
        plot_handler.scatter(x_values, sample_c, 'Sample C')

        plot_handler.add_1to1_line()

        plot_handler.annotate_plot()

        show(plot_handler)

        # save results
        save(plot_handler, 'html')
        save(plot_handler, 'png')

    print('Done')
