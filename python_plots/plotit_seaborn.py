""" seaborn Wrapper

Documentation: <https://seaborn.pydata.org/examples/index.html>

"""
import seaborn as sns
import matplotlib.pyplot as plt


from python_plots.plotit import PlotHandlerFrame, combine_frame


class SeabornHandler(PlotHandlerFrame):

    @property
    def handler_name(self) -> str:
        return 'seaborn'

    # Methods #
    ###########

    def line(self, xx, yy, label: str = None):
        self.data_source, x_label, y_label = combine_frame(self.data_source, xx, yy)
        self._figure_handle = sns.lineplot(x=x_label, y=y_label, data=self.data_source)

    def scatter(self, xx, yy, label: str = None):
        self.data_source, x_label, y_label = combine_frame(self.data_source, xx, yy)
        self._figure_handle = sns.scatterplot(x=x_label, y=y_label, data=self.data_source)

    def annotate_plot(self) -> None:
        self.plot_ready()
        self.update_data()
        self.figure_handle.set(title=self.description_str,
                               xlabel=self.x_axis_title,
                               ylabel=self.y_axis_title)
        return

    def save(self, file_name: str) -> str:
        figure = self.figure_handle.get_figure()
        figure.savefig(file_name)
        return file_name

    def show(self) -> None:
        print(' seaborn is using matplotlib to show.')
        plt.show()
        return
