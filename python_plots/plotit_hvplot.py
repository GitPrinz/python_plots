""" hvPlot Wrapper

Documentation: <https://hvplot.holoviz.org/user_guide/Plotting.html>

"""

import hvplot
import hvplot.pandas  # noqa
import holoviews as hv


from python_plots.plotit import PlotHandlerFrame, combine_frame


class HVPlotHandler(PlotHandlerFrame):

    @property
    def handler_name(self) -> str:
        return 'hvPlot'

    # Methods #
    ###########

    def line(self, xx, yy, label: str = None):
        self.data_source, x_label, y_label = combine_frame(self.data_source, xx, yy)
        self.append2figure_handle(self.data_source.hvplot.line(x=x_label, y=y_label))

    def scatter(self, xx, yy, label: str = None):
        self.data_source, x_label, y_label = combine_frame(self.data_source, xx, yy)
        self.append2figure_handle(self.data_source.hvplot(x=x_label, y=y_label, kind='scatter'))

    def annotate_plot(self) -> None:
        self.plot_ready()
        self.update_data()
        self.figure_handle.opts(title=self.description_str)
        print(' Unclear how to change the axis label in hvPlot.')
        return

    def show(self) -> None:
        print(' hvPlot will use holoviews to render.')
        hv.render(self.figure_handle)

    def save(self, file_name: str) -> str:
        hvplot.save(self.figure_handle, file_name)
        return file_name
