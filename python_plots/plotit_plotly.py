""" Plotly Wrapper

Documentation: <https://plotly.com/python/>

"""

import plotly.graph_objects as go
import plotly.offline as po


from python_plots.plotit import PlotHandler


class PlotlyHandler(PlotHandler):

    @property
    def handler_name(self) -> str:
        return 'Plotly'

    def prepare_plot(self) -> go.Figure:
        self._figure_handle = go.Figure()
        return self.figure_handle

    def line(self, xx, yy, label: str = None):
        self.figure_handle.add_trace(go.Scatter(x=xx, y=yy, mode='lines', name=label))

    def scatter(self, xx, yy, label: str = None):
        self.figure_handle.add_trace(go.Scatter(x=xx, y=yy, mode='markers', name=label))

    def get_min_max(self) -> tuple:
        """ return minimum and maximum of plotted data """
        xmins = []
        xmaxs = []
        for plot_data in self.figure_handle.data:
            xmins.append(min(plot_data.x))
            xmaxs.append(max(plot_data.x))
        return min(xmins) if len(xmins) > 0 else 0, max(xmaxs) if len(xmaxs) > 0 else 1

    def annotate_plot(self) -> None:
        self.plot_ready()
        self.update_data()
        self.figure_handle.update_layout(title=self.description_str,
                                         xaxis_title=self.x_axis_title,
                                         yaxis_title=self.y_axis_title)
        return

    def save(self, file_name: str) -> str:
        if file_name[-5:] == '.html':
            return po.plot(self.figure_handle, filename=file_name, auto_open=False)
        else:
            raise ValueError('Would need a lot of Packages ...')
