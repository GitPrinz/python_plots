import numpy as np
import pandas as pd

from typing import Union

from abc import ABC, ABCMeta, abstractmethod


def normalize(values_orig: Union[list, np.ndarray], values_pred: Union[list, np.ndarray]):
    """ Scale lists to 0-1 (regarding first list). """
    min_val = np.nanmin(values_orig)
    dif_val = np.nanmax(values_orig) - min_val
    return [(val - min_val) / dif_val for val in values_orig], [(val - min_val) / dif_val for val in values_pred]


def prepare_frame(xx, yy) -> tuple:
    """ Prepare Dataframe and return column_names"""
    if isinstance(xx, pd.Series):
        x_label = 'x' if xx.name is None else xx.name
    else:
        x_label = 'x'
        xx = pd.Series(xx, name=x_label)
    if isinstance(yy, pd.Series):
        y_label = 'y' if yy.name is None else yy.name
    else:
        y_label = 'y'
        yy = pd.Series(yy, name=y_label)
    return pd.DataFrame([xx, yy]).T, x_label, y_label


def combine_frame(data_frame: pd.DataFrame, xx, yy) -> tuple:
    new_frame, x_label, y_label = prepare_frame(xx, yy)

    ori_x = x_label
    ori_y = y_label

    while x_label in data_frame:
        x_label += 'x'
    while y_label in data_frame:
        y_label += 'y'

    new_frame.rename(columns={ori_x: x_label, ori_y: y_label}, inplace=True)

    return data_frame.join(new_frame, how='outer'), x_label, y_label


class PlotHandler(metaclass=ABCMeta):
    """ Basics for Plotting with different tools.

    Example:

        - PH_object.scatter(xx1, yy1)
        - PH_object.scatter(xx2, yy2)
        - PH_object.add_1to1_line()
        - PH_object.annotate_plot()
        - PH_object.show()
    """

    #: get actual plot data todo: kind of weired
    update_description_method: callable = None

    # descriptive data
    description_str: str = None
    x_axis_title: str = None
    y_axis_title: str = None

    # plot details
    _figure_handle = None
    _1to1_handle = None

    def __init__(self, get_description: callable = None):
        """
        :param get_description: method which returns tuple with

            - description_str
            - x_axis_title
            - y_axis_title

        """
        self.update_description_method = get_description
        return

    # Properties #
    ##############

    @property
    def handler_name(self) -> str:
        return 'Unknown'

    @property
    def figure_handle(self):
        if self._figure_handle is None:
            self.prepare_plot()
        return self._figure_handle

    # Methods #
    ###########

    def update_data(self) -> None:
        """ Try to update the plot data.

        This is used to always have the newest data and can't forget to change it. A little bit strange. """
        if self.update_description_method is not None:
            self.description_str, self.x_axis_title, self.y_axis_title = \
                self.update_description_method()
        if self.description_str is None:
            self.description_str = 'Plot'
        if self.x_axis_title is None:
            self.x_axis_title = 'X Axis'
        if self.y_axis_title is None:
            self.y_axis_title = 'Y Axis'
        return

    def show(self) -> None:
        # this has to be overwritten if there is a different style
        self.figure_handle.show()

    def plot_ready(self) -> None:
        """ Make sure all necessary variables are initialized for plotting. """
        if self.figure_handle is None:
            raise Exception('Figure handle could not be initialized')
        return

    def add_1to1_line(self, data_range: tuple = None) -> None:
        """ Add a 1to1 line to the plot. """
        if data_range is None:
            xx = list(self.get_min_max())
        else:
            xx = list(data_range)
        self._1to1_handle = self.line(xx, xx, label='1:1')
        return

    def update_1to1_line(self, data_range: tuple = None) -> None:
        if self._1to1_handle is not None:
            del self._1to1_handle
        self.add_1to1_line(data_range=data_range)

    # Abstract Methods #
    ####################

    @abstractmethod
    def prepare_plot(self):
        pass

    @abstractmethod
    def line(self, xx: Union[list, pd.Series, np.ndarray], yy: Union[list, pd.Series, np.ndarray],
             label: str = None) -> None:
        pass

    @abstractmethod
    def scatter(self, xx: Union[list, pd.Series, np.ndarray], yy: Union[list, pd.Series, np.ndarray],
                label: str = None) -> None:
        pass

    @abstractmethod
    def get_min_max(self) -> tuple:
        """ return minimum and maximum of plotted data """
        pass

    @abstractmethod
    def annotate_plot(self) -> None:
        """ Use stored labels to update plot data """
        pass

    @abstractmethod
    def save(self, file_name: str) -> str:
        """ Save current plot and return filename"""
        pass

    # Further Supporting Methods #
    ##############################

    def append2figure_handle(self, new_component) -> None:
        """ Handle the empty figure_handle case.

        It would be nice to have a compatible dummy element to make this unnecessary.
        """
        if self._figure_handle is None:
            self._figure_handle = new_component
        else:
            self._figure_handle *= new_component
        return

    def add2figure_handle(self, new_component) -> None:
        """ Handle the empty figure_handle case.

        It would be nice to have a compatible dummy element to make this unnecessary.
        """
        if self._figure_handle is None:
            self._figure_handle = new_component
        else:
            self._figure_handle += new_component
        return


class PlotHandlerFrame(PlotHandler, ABC):
    """ Subclass for using pandas DataFrame as source for plot data. """

    #: DataFrame as Source for Plotting
    data_source: pd.DataFrame = None

    def prepare_plot(self) -> None:
        self.data_source = pd.DataFrame()
        return

    def get_min_max(self) -> tuple:
        """ return minimum and maximum of plotted data """
        xmin = self.data_source.min().min()
        xmax = self.data_source.max().max()
        return xmin if xmin.size > 0 else 0, xmax if xmax.size > 0 else 1
