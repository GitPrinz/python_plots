""" Matplotlib Wrapper

Examples: <https://matplotlib.org/gallery/index.html>

"""

import matplotlib.pyplot as plt

from python_plots.plotit import PlotHandler


class MatplotlibHandler(PlotHandler):

    _axis_handle = None

    # Properties #
    ##############

    @property
    def handler_name(self) -> str:
        return 'Matplotlib'

    @property
    def axis_handle(self):
        if self._axis_handle is None:
            self.prepare_plot()
        return self._axis_handle

    # Methods #
    ###########

    def prepare_plot(self):
        self._figure_handle, self._axis_handle = plt.subplots()

    def line(self, xx, yy, label: str = None) -> None:
        self.plot_ready()
        self.axis_handle.plot(xx, yy)

    def scatter(self, xx, yy, label: str = None) -> None:
        self.plot_ready()
        self.axis_handle.scatter(xx, yy)

    def get_min_max(self) -> tuple:
        """ return minimum and maximum of plotted data """
        xmins = []
        xmaxs = []
        for line in self.axis_handle.lines:
            xmins.append(min(min(line.get_xydata())))
            xmaxs.append(max(max(line.get_xydata())))
        return min(xmins) if len(xmins) > 0 else 0, max(xmaxs) if len(xmaxs) > 0 else 1

    def annotate_plot(self) -> None:
        self.plot_ready()
        self.axis_handle.set_xlabel(r'$\Delta_i$')
        self.axis_handle.set_ylabel(r'$\Delta_{i+1}$')
        self.axis_handle.set_title('Volume and percent change')
        pass

    def save(self, file_name: str) -> str:
        self.figure_handle.savefig(file_name)
        return file_name
