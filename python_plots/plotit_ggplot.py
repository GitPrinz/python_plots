""" ggplot2 / plotnine Wrapper

Examples: <http://r-statistics.co/Top50-Ggplot2-Visualizations-MasterList-R-Code.html>
Examples: <https://www.r-graph-gallery.com/ggplot2-package.html>

"""

from plotnine import ggplot, aes, geom_line, geom_point, labs, ggsave

from python_plots.plotit import PlotHandlerFrame, combine_frame


class GGPlotHandler(PlotHandlerFrame):

    # Properties #
    ##############

    @property
    def handler_name(self) -> str:
        return 'ggplot'

    # Methods #
    ###########

    def prepare_plot(self) -> None:
        super().prepare_plot()
        self._figure_handle = ggplot(self.data_source)
        return

    def line(self, xx, yy, label: str = None) -> None:
        self.plot_ready()
        self.data_source, x_label, y_label = combine_frame(self.data_source, xx, yy)
        self._figure_handle += geom_line(aes(x=x_label, y=y_label))

    def scatter(self, xx, yy, label: str = None) -> None:
        self.plot_ready()
        self.data_source, x_label, y_label = combine_frame(self.data_source, xx, yy)
        self._figure_handle += geom_point(aes(x=x_label, y=y_label))

    def annotate_plot(self) -> None:
        self.plot_ready()
        self.add2figure_handle(labs(title=self.description_str, x=self.x_axis_title, y=self.y_axis_title))

    def show(self) -> None:
        print(' ggplot is using matplotlib')
        self.figure_handle.draw()
        return

    def save(self, file_name: str) -> str:
        ggsave(file_name)
        return file_name
