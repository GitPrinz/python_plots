""" HoloViews Wrapper

HoloViews is itself a wrapper for Bokeh, Matplotlib and Plotly.

    - Bokeh needs selenium and fails with the driver for the browser.
    - matplotlib doesn't work?
    - plotly doesn't fail, but i didn't see anything.

Documentation: <https://holoviews.org/reference/index.html>

"""

import holoviews as hv


from python_plots.plotit import PlotHandler


class HoloviewsHandler(PlotHandler):

    #: Render engine ('bokeh', 'plotly', 'matplotlib')
    backend: str = 'plotly'

    @property
    def handler_name(self) -> str:
        return 'HoloViews'

    # Methods #
    ###########

    def prepare_plot(self) -> None:
        pass

    def line(self, xx, yy, label: str = None):
        self.append2figure_handle(hv.Curve(list(zip(xx, yy))))

    def scatter(self, xx, yy, label: str = None):
        self.append2figure_handle(hv.Scatter(list(zip(xx, yy))))

    def get_min_max(self) -> tuple:
        """ return minimum and maximum of plotted data """
        xmin = [0]
        xmax = [1]
        # todo
        return xmin if len(xmin) > 0 else 0, xmax if len(xmax) > 0 else 1

    def annotate_plot(self) -> None:
        self.plot_ready()
        self.update_data()
        self.figure_handle.opts(title=self.description_str,
                                xlabel=self.x_axis_title,
                                ylabel=self.y_axis_title)
        return

    def show(self) -> None:
        hv.render(self.figure_handle, backend=self.backend)

    def save(self, file_name: str) -> str:
        hv.save(self.figure_handle, file_name, backend=self.backend)
        return file_name
