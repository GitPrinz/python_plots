""" Altair Wrapper

Documentation: <https://altair-viz.github.io/gallery/index.html>

Needs altair_viewer to show()

"""

import pandas as pd
import altair as alt


from python_plots.plotit import PlotHandler, combine_frame


class AltairHandler(PlotHandler):

    data_source: pd.DataFrame = None

    @property
    def handler_name(self) -> str:
        return 'altair'

    # Methods #
    ###########

    def prepare_plot(self) -> None:
        self.data_source = pd.DataFrame()
        return

    def line(self, xx, yy, label: str = None):
        self.data_source, x_label, y_label = combine_frame(self.data_source, xx, yy)
        self.add2figure_handle(alt.Chart(self.data_source).mark_line().encode(x=x_label, y=y_label).interactive())

    def scatter(self, xx, yy, label: str = None):
        self.data_source, x_label, y_label = combine_frame(self.data_source, xx, yy)
        self.add2figure_handle(alt.Chart(self.data_source).mark_circle().encode(x=x_label, y=y_label).interactive())

    def get_min_max(self) -> tuple:
        """ return minimum and maximum of plotted data """
        xmin = self.data_source.min().min()
        xmax = self.data_source.max().max()
        return xmin if xmin.size > 0 else 0, xmax if xmax.size > 0 else 1

    def annotate_plot(self) -> None:
        self.plot_ready()
        self.update_data()
        # todo: this might be wrong
        self.figure_handle.encode(column=self.description_str,
                                  x=self.x_axis_title,
                                  y=self.y_axis_title)
        return

    def save(self, file_name: str) -> str:
        self.figure_handle.save(file_name)
        return file_name

    def show(self):
        try:
            self.figure_handle.show()
        except ValueError:
            print('Show is not possible for Altair without altair_viewer package.')
