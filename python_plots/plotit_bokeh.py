""" Bokeh Wrapper

Documentation: <https://docs.bokeh.org/en/latest/docs/gallery.html>

More complex, but very flexible.

"""

import warnings

import numpy as np

from bokeh import resources
from bokeh.plotting import figure, show, save
from bokeh.models import ColumnDataSource
from bokeh.palettes import colorblind

from python_plots.plotit import PlotHandler


class BokehHandler(PlotHandler):

    _glyphs = []
    _next_glyph = 0

    @property
    def handler_name(self) -> str:
        return 'Bokeh'

    @property
    def figure_handle(self):
        if self._figure_handle is None:
            self.prepare_plot()
        return self._figure_handle

    @figure_handle.setter
    def figure_handle(self, new_handle: figure) -> None:
        self._figure_handle = new_handle
        self.initialize_glyphs()

    def prepare_plot(self) -> figure:
        self._figure_handle = figure(tooltips=[("Real", " $x (@x)"), ("Predicted", " $y (@y)")])
        self.initialize_glyphs()
        return self.figure_handle

    def line(self, xx, yy, label: str = None):
        self.plot_ready()
        if label is None:
            label = 'Plot'
        self.figure_handle.line(x=xx, y=yy, legend_label=label)
        return

    def scatter(self, xx, yy, label: str = None):
        self.plot_ready()
        if label is None:
            label = 'Plot'
        # todo: improve glyph handling - this is old and crazy
        self.update_next_glyph(glyph_data={'x': xx, 'y': yy}, glyph_label=label)
        return

    def add_1to1_line(self, data_range: tuple = None) -> None:
        self.plot_ready()
        if not self.update_1to1_line_source(data_range):
            self.figure_handle.line(x='x', y='y', source=self._1to1_handle, legend_label='1:1')

    def update_1to1_line(self, data_range: tuple = None) -> None:
        self.add_1to1_line(data_range=data_range)

    def get_min_max(self) -> tuple:
        """ return minimum and maximum of plotted data """
        xmins = []
        xmaxs = []
        for renderer in self.figure_handle.renderers:
            if len(renderer.data_source.data['x']) > 0:
                xmins.append(np.nanmin(renderer.data_source.data['x']))
                xmaxs.append(np.nanmax(renderer.data_source.data['x']))
        return min(xmins) if len(xmins) > 0 else 0, max(xmaxs) if len(xmaxs) > 0 else 1

    def annotate_plot(self) -> None:
        self.plot_ready()
        self.update_data()
        self.figure_handle.title.text = self.description_str
        self.figure_handle.xaxis.axis_label = self.x_axis_title
        self.figure_handle.yaxis.axis_label = self.y_axis_title
        return

    def show(self, figure_handle=None) -> None:
        show(self.figure_handle if figure_handle is None else figure_handle)

    def save(self, file_name: str) -> str:
        return save(self.figure_handle, filename=file_name, title=file_name, resources=resources.INLINE)

    # Support Functions #
    #####################

    def update_1to1_line_source(self, data_range: tuple) -> bool:
        """ Add a 1to1 line to the plot. """
        if data_range is None:
            data_range = list(self.get_min_max())
        line_dict = {'x': data_range, 'y': data_range}
        if self._1to1_handle is None:
            self._1to1_handle = ColumnDataSource(line_dict)
            return False
        else:
            self._1to1_handle.data = line_dict
            return True

    def initialize_glyphs(self):
        """ Create a glyph for each available color. """
        for num, color in enumerate(colorblind['Colorblind'][8]):
            new_source = ColumnDataSource({'x': [], 'y': []})
            self._glyphs.append({'source': new_source,
                                 'glyph': self.figure_handle.scatter(
                                     x='x', y='y', source=new_source,
                                     visible=False,
                                     legend_label='Plot {}'.format(num + 1),
                                     color=color)})
        return

    @staticmethod
    def update_glyph(glyph_dict: dict, glyph_data: dict, glyph_label: str) -> None:
        glyph_dict['source'].data = glyph_data
        # todo: find out how to change legend and make legend interactive
        glyph_dict['glyph'].glyph.fill_alpha = 1 / np.log(len(
            glyph_dict['source'].data['x']) / 2 + 10)
        glyph_dict['glyph'].glyph.line_alpha = (1 + 1 / np.log(len(
            glyph_dict['source'].data['x']) / 2 + 10)) / 2
        glyph_dict['glyph'].visible = True
        return

    def update_next_glyph(self, glyph_data: dict, glyph_label: str) -> None:
        self.plot_ready()
        self.update_glyph(self._glyphs[self._next_glyph], glyph_data=glyph_data, glyph_label=glyph_label)
        if self._next_glyph < len(self._glyphs) - 1:
            self._next_glyph += 1
        else:
            warnings.warn("Can only display {} different glyphs. Will start to overwrite with next input!".format(
                len(self._glyphs)))
            self._next_glyph = 0
        return

    def reset_glyphs(self):
        for glyph in self._glyphs:
            glyph['glyph'].visible = False
        self._next_glyph = 0
