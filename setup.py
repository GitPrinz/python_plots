import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="python_plots",
    version="0.1.0",
    author="Martin Prinzler",
    author_email="git@martin-prinzler.de",
    description="Useless one fits all plot handler",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/GitPrinz/python_plots",
    project_urls={
        "Bug Tracker": "https://gitlab.com/GitPrinz/python_plots/-/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Development Status :: 3 - Alpha"
    ],
    packages=['python_plots'],
    python_requires=">=3.6",
    zip_safe=False
)
