# Python Plot Readme

Some scripts to play with and understand different Plotting methods in their basic concepts.
In plotit_x there are mappers to have a common interface for the different functions.

The Following wrappers are implemented:

- [Bokeh](python_plots/plotit_bokeh.py)
- [Matplotlib](python_plots/plotit_matplotlib.py)
- [Plotly](python_plots/plotit_plotly.py)

The following wrappers are on the ToDo List, but I dond't plan to update this repository in the future.:

- hvplot
- altair
- dashboard
- holoview
- seaborn

See 

## History

### Version 0.1.0 (Packaging)

- Switched to package structure