# Phony will be executed no matter what.
.PHONY: doc
.DEFAULT_GOAL := help
###########################################################################################################
## SCRIPTS
###########################################################################################################

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
        match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
        if match:
                target, help = match.groups()
                print("%-20s %s" % (target, help))
endef

###########################################################################################################
## VARIABLES
###########################################################################################################

export PRINT_HELP_PYSCRIPT
ifeq ($(OS),Windows_NT)
    export PYTHON=py
else
    export PYTHON=python3
endif


###########################################################################################################
## ADD TARGETS FOR YOUR TASK
###########################################################################################################

export PRINT_HELP_PYSCRIPT

help:
	$(PYTHON) -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

install:  ## Install this package
	$(MAKE) install-requirements
	$(MAKE) install-package

install-requirements:
	$(PYTHON) -m pip install -r requirements.txt

install-package:
	$(PYTHON) -m pip install .

demo:  ## Run example script
	$(PYTHON) example/example_plots.py